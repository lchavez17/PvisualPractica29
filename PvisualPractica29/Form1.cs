﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvisualPractica29
{
    public partial class Form1 : Form
    {
        string cadena = "";
        public Form1()
        {
            InitializeComponent();
        }
        private void limpiarCaja()
        {
            textBox1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.TextLength > 0)
            {
                if (checkBox1.Checked)
                {
                    cadena +="HOLA "+ textBox1.Text + Environment.NewLine;
                    if (radioButton1.Checked) cadena += "TU GENERO ES MASCULINO" + Environment.NewLine;
                    if (radioButton2.Checked) cadena += "TU GENERO ES FEMENINO" + Environment.NewLine;
                    if (radioButton3.Checked) cadena += "TU EDAD ES MENOR DE 18" + Environment.NewLine;
                    if (radioButton4.Checked) cadena += "TU EDAD ES ENTRE 18 Y 50" + Environment.NewLine;
                    if (radioButton5.Checked) cadena += "TU EDAD ES MAYOR DE 50" + Environment.NewLine;
                }
                else if (checkBox1.Checked == false)
                {
                    cadena +="Hola "+ textBox1.Text + Environment.NewLine;
                    if (radioButton1.Checked) cadena += "Tu Genero es Masculino" + Environment.NewLine;
                    if (radioButton2.Checked) cadena += "Tu Genero es Femenino" + Environment.NewLine;
                    if (radioButton3.Checked) cadena += "Tu Edad es Menor de 18" + Environment.NewLine;
                    if (radioButton4.Checked) cadena += "Tu Edad es Entre 18 Y 50" + Environment.NewLine;
                    if (radioButton5.Checked) cadena += "Tu Edad es Mayor 50" + Environment.NewLine;
                }
                textBox2.Text = cadena;
                limpiarCaja();
            }
        }
    }
}
